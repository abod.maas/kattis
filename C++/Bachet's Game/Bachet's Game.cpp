#include <iostream>
#include <sstream>

//LiuID:abena406

using namespace std;

class BachetGame{

  public:

  int first_player = 1;
  int second_player = 2;
  int stones_num;
  int *removing_variation;
  int *stones;
  
  int possible_Takes_number;

  BachetGame(int stones_max, int possible_Takes_num){
    stones = new int[stones_max+1];
    stones[0] = second_player;

    for (int i = 1; i <= stones_max ; i++ ){
      stones[i] = 0;
    }
    stones_num = stones_max;
    removing_variation = new int[possible_Takes_num];
    possible_Takes_number = possible_Takes_num;
  };

  void addVariation(int move, int pos){  
    removing_variation[pos] = move;  
  }
  //Print winner on terminal
  void getWinner(int player){
    if (player == first_player){
      cout << "Stan wins" << endl;
    }
    else{
      cout << "Ollie wins" << endl;
    }
  }

  void startGame(){

     for (int i = 0; i <= stones_num; i++)
    {
        if (stones[i] == 0){
          stones[i] = second_player;
        }
        if (stones[i] == second_player)
        { //dynamic building.
                for (int take_amount = 0; take_amount < possible_Takes_number ; take_amount++)
                {
                    if (i + removing_variation[take_amount] <= stones_num) stones[i + removing_variation[take_amount]] = first_player;
            }
        }
    }
    getWinner(stones[stones_num]);
  }

};


int main() {
  int stones_number;
  int take_variation;
  int possible_take;
  bool running; 
  while(cin >> stones_number >> take_variation){

    BachetGame game = BachetGame(stones_number,take_variation );

  for(int loop = 0; loop < take_variation; loop++){
      cin >> possible_take;
      game.addVariation(possible_take, loop);
  }

  game.startGame();
    }
    
  }
