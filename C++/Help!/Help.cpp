#include <iostream>
#include <fstream>
#include <string>
#include <sstream>



/**

@author Abedalhkeem Najeeb (LIU: abena406)

**/

using namespace std;
bool isPlaceHolder(string& str){
    return (str[0] == '<' && str[str.size()-1] == '>');
}

void find_phrase(string* pattern1, string* pattern2, int length){
    string word1;
    string word2;
    for( int i=0; i<length;i++){
        word1 = pattern1[i];
        word2 = pattern2[i];
        if(isPlaceHolder(word1) && !isPlaceHolder(word2)){
            // Replace all placeholders word1 with word2 
            for(int j = 0; j<length; j++){
                if( pattern1[j] == word1){
                    pattern1[j] = word2;
                }
            }
            return find_phrase(pattern1,pattern2,length);
        }
        else if(!isPlaceHolder(word1) && isPlaceHolder(word2)){
            // Replace all placeholders word1 with word2 
            for(int j = 0; j<length; j++){
                if( pattern2[j] == word2){
                    pattern2[j] = word1;
                }
            }
            return find_phrase(pattern1,pattern2,length);
        }
    }
    bool all_equal = true;
    for( int i=0; i<length;i++){
        word1 = pattern1[i];
        word2 = pattern2[i];
        if(isPlaceHolder(word1) && isPlaceHolder(word2) ){
            pattern1[i] = "supbro";
            pattern2[i] = "supbro";
            return find_phrase(pattern1,pattern2,length);}
        if(word1 != word2){
            all_equal = false;}
            }
    if(all_equal){
        for( int i=0; i<length;i++){
            cout << pattern1[i] << " ";}
            cout << endl;}
    else{
        cout << "-" << endl;
    }    
}

int main(){

    //ifstream myfile;
    //myfile.open("sample.in");
    string loop;
    string str1;
    string str2;
    string word1;
    string word2;
    int count = 0;
    getline(cin,loop);
    for(int i = 0; i<stoi(loop);i++){

        count = 0;
        string pattern1[100];
        string pattern2[100];
        getline(cin,str1,'\n');
        getline(cin,str2,'\n');
        stringstream ss1(str1);
        stringstream ss2(str2);
        while (ss1 >> word1 && ss2 >> word2){
            pattern1[count]=word1;
            pattern2[count]=word2;
            count++;
        }
        if(ss1.eof() != true || ss2.eof() != true){
            cout << "-" << endl;
            return 0;
        }
        find_phrase(pattern1, pattern2, count);}
    return 0;
}