#include <iostream>
#include <fstream>
#include <cstdio>
#include <set>
#include <cstdio>


/**
 * @author : Abedalhkeem Najeeb (LIU : abena406)
 */


using namespace std;
int main(){
    ifstream myfile;
    //myfile.open("sample.in");
    unsigned int loop;
    //myfile >> loop;
    scanf("%u", &loop);
    unsigned int v[loop+1] = {};
    unsigned int d[loop+2] = {};
    unsigned int solution[loop+1] = {};
    unsigned int node;

    set<unsigned int , greater<unsigned int>> s;
    for(unsigned int i = 0; i < loop ; i++){
        //myfile >>node;
        scanf("%u", &node);
        d[node] += 1; 
        v[i] = node;}
    
    //A solution is not possible when “last input” ≠ n + 1.
    if(v[loop-1] != loop+1){
        //cout << "Error" << endl;
        printf("%s", "Error");
        return 0;
    }

    // Save all nodes with degree 0. Degree 0 it means it is leaf or it is independet.
    for(int j= 1; j < loop+2; j++){
        if(d[j]==0){
            s.insert(j);}}
    // Take least node num and update s.
    for(unsigned int i =0; i < loop ; i++){
        node = *prev(s.end());
        //cout << node << endl;
        printf("%u\n", node);
        s.erase(prev(s.end()));
        d[v[i]] -= 1;
        if(d[v[i]] == 0){
            s.insert(v[i]);
        }
    }
    

return 0;
}