#include <cstdio>

int main() {

    while (true) {
        // Read entry count
        unsigned int n, m;
        scanf("%u %u", &n, &m);
        
        // Check if terminator
        if (n == 0 && m == 0) {
            break;
        }

        bool* CD = new bool[100000001]();

        int x;
        for (int i = 0; i < n; ++i) {
            scanf("%u", &x);
            CD[x] = true;
        }

        int counter = 0;
        for (int i = 0; i < m; ++i) {
            scanf("%u", &x);
            if (CD[x]) {
                ++counter;
            }
        }

        printf("%u\n", counter);

        delete[] CD;
    }

    return 0;
}