import sys


alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
dict = {}
for alpha in alphabets:
    dict[alpha] = [False, 0, 0]

if __name__ == '__main__':
    #with open("sample/2.in") as file:
        #file = file.readlines()
    file = sys.stdin.readlines()
    for sub in file[:-1]:
        submit = sub.split()
        if not dict[submit[1]][0]:
            if submit[2][-1] == 'g':
                dict[submit[1]][1] += 1
            else:
                dict[submit[1]][2] = int(submit[0])
                dict[submit[1]][0] = True

    total_time = 0
    passed_number = 0
    for alpha in alphabets:
        if dict[alpha][0]:
            passed_number += 1
            total_time += dict[alpha][2] + dict[alpha][1] * 20

    print(passed_number, total_time)