import sys

game = {}


def reset():
    game["w"] = [0]
    game["h"] = [11]


if __name__ == '__main__':
    reset()
    #transcript = sys.stdin.readlines()
    transcript = open("sample/1.in").readlines()
    for line in range(0, len(transcript[:-1]), 2):

        letter = transcript[line+1][-2]

        if letter != "n":
            game[letter].append(int(transcript[line][:-1]))
        else:
            num = int(transcript[line][:-1])
            if max(game["w"]) < num < min(game["h"]):
                print("Stan may be honest")
            else:
                print("Stan is dishonest")
            reset()

        #print(transcript[line][:-1])




