import sys

#file = open("sample/1.in",'r').read().splitlines()
#file = open("sample/2.in").readlines()
file = sys.stdin.readlines()
stones = int(file[0])
for n in range(50000):
    if ((4*n**3)+(12*n**2)+(11*n)+3) > (3*stones):
        solution = n
        break

print(solution )
