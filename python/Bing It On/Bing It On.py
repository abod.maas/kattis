import sys

class Node():
    def __init__(self, letter):
        self.letter = letter
        self.children = []
        self.count = 1

    def find_children(self, letter):
        for child in self.children:
            if child.letter == letter:
                return child


if __name__ == '__main__':
    lines = sys.stdin.readlines()
    #lines = open("sample/1.in").readlines()
    node = Node("0")

    for word in lines[1:]:
        current_node = node
        not_found = False
        for letter in word[:-1]:
            if not not_found:
                child = current_node.find_children(letter)

            if child is not None and not not_found:
                child.count += 1
                current_node = child
            else:
                not_found = True
                new_node = Node(letter)
                current_node.children.append(new_node)
                current_node = new_node

        print(current_node.count-1)



