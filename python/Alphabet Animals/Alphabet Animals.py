import sys

alphabets = "abcdefghijklmnopqrstuvwxyz"


if __name__ == '__main__':

        file = sys.stdin.readlines()
        last_letter = file[0][-2]
        dict = {}
        printed = False
        for i in range(len(alphabets)):
            dict[alphabets[i]] = []

        for i in range(int(file[1][:-1])):
            dict[file[2+i][0]].append(file[2+i])

        if not dict[last_letter]:
            print("?")
        else:

            if len(dict[last_letter]) == 1 and  dict[last_letter][0][-2]== last_letter:
                print(dict[last_letter][0][:-1]+"!")
                printed = True
            else:
                for word in dict[last_letter]:
                    if not dict[word[-2]]:
                        print(word[:-1]+"!")
                        printed = True
                        break
            if not printed:
                print(dict[last_letter][0][:-1])
