import sys

if __name__ == '__main__':
    #file = sys.stdin.readlines()
    file = open("sample/1.in").readlines()
    number_of_printer = 1
    jobs = int(file[0][:-1])
    best = jobs
    temp = 0
    count = 0
    while True:
        number_of_printer += number_of_printer
        count += 1
        temp = (jobs / number_of_printer) + (count)
        if temp < best:
            best = temp
        else:
            break
    if int(best) < best:
        print(int(best)+1)
    else:
        print(int(best))