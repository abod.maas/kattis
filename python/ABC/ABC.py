import sys

dict = {"A":0, "B" : 0, "C":0}

if __name__ == '__main__':
    #file = open("sample/1.in").readlines()
    file = sys.stdin.readlines()
    numbers = file[0].split()
    numbers[0], numbers[1], numbers[2] = int(numbers[0]), int(numbers[1]), int(numbers[2])
    dict["A"] = min(numbers)
    dict["C"] = max(numbers)

    A = False
    C = False

    for number in numbers:
        if number == dict["A"] and not A:
            A = True
        elif number == dict["C"] and not C:
            C = True
        else:
            dict["B"] = number
            break
            
    print(dict[file[1][0]], dict[file[1][1]], dict[file[1][2]])