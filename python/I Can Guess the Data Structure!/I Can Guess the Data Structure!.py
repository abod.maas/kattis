import sys

if __name__ == '__main__':
    file = open("sample/guessthedatastructure_sample.in").readlines()
    # file = sys.stdin.readlines()

    line_to_check = 0

    for data in file:

        if " " not in data:
            line_to_check = int(data)
            priority = True
            stack = True
            queue = True
            to_continue = True

            lst_stack = []
            lst_queue = []
            lst_priority = []

        else:

            data = data.split()
            num = int(data[1])

            if data[0] == "1":
                lst_stack.append(num)
                lst_queue.append(num)
                lst_priority.append(num)
                line_to_check -= 1

                if line_to_check == 0:
                    if not to_continue:
                        print("impossible")
                    elif sum([priority, stack, queue]) == 0:
                        print("impossible")
                    elif sum([priority, stack, queue]) > 1:
                        print("not sure")
                    elif stack:
                        print("stack")
                    elif queue:
                        print("queue")
                    else:
                        print("priority queue")

                continue

            if not to_continue:

                line_to_check -= 1

                if line_to_check == 0:
                    print("impossible")

                continue

            if not any([num in lst_stack, num in lst_queue, num in lst_priority]):
                to_continue = False
                line_to_check -= 1
                if line_to_check == 0:
                    print("impossible")

                continue

            if stack:
                if lst_stack:
                    if not (lst_stack.pop() == num):
                        stack = False

            if queue:
                if lst_queue:
                    if not lst_queue.pop(0) == num:
                        queue = False

            if priority:
                if lst_priority:
                    if not max(lst_priority) == num:
                        priority = False
                    if num in lst_priority:
                        lst_priority.remove(num)

            line_to_check -= 1

            if line_to_check == 0:
                if sum([priority, stack, queue]) == 0:
                    print("impossible")
                elif sum([priority, stack, queue]) > 1:
                    print("not sure")
                elif stack:
                    print("stack")
                elif queue:
                    print("queue")
                else:
                    print("priority queue")
