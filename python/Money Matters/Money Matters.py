import sys


def get_money(parent):
    global needed
    check_list[parent] = 1
    for friend in friends_list[parent]:
        if needed == 0:
            return
        if not check_list[friend]:
            if money_list[friend] > 0:
                if not money_list[friend] < (-needed):
                    money_list[friend] = money_list[friend] + needed
                    needed = 0
                    status[0] = True
                    return
                else:
                    needed = needed + money_list[friend]
                    money_list[friend] = 0
            get_money(friend)





if __name__ == '__main__':

    #lines = open("sample/money_sample.00.in").readlines()
    lines = sys.stdin.readlines()
    #print(lines[0].split()[0])
    money = int(lines[0].split()[0])
    money_list = []
    friends_list = []
    check_sample = []
    for money_owned in range(money):
        money_list.append(int(lines[money_owned+1][:-1]))
        friends_list.append([])
        check_sample.append(0)

    for friend in lines[money+1:]:
        friend = friend.split()
        friends_list[int(friend[0])].append(int(friend[1]))
        friends_list[int(friend[1])].append(int(friend[0]))

    for person in range(len(money_list)):

        if money_list[person] < 0:
            check_list = check_sample.copy()
            status = [False]
            needed = money_list[person]
            get_money(person)
            if not status[0]:
                print("IMPOSSIBLE")
                exit()
            else:
                status[0] = False
    print("POSSIBLE")