import sys

height = (2 ** (-3/4))
width = (2 ** (-5 / 4))


def get_param(level):

    if level == 1:
        return height, width
    if isinstance(level/2, int):
        return width / (2 ** (level - 2)/2), height / (2**(level/2))
    else:
        return height / (2 ** ((level - 1)/2)), width / (2 ** ((level - 1)/2))


def check_possible(lst, needed, level, sum):

    if not lst:
        print("impossible")
        return
    if not needed > lst[0]:
        h, w = get_param(level)
        sum += ((needed / 2) * h)
        print(sum)
        return
    else:

        h, w = get_param(level)

        dif = needed - lst[0]
        sum += ((needed/2) * h)
        needed = dif * 2

        check_possible(lst[1:], needed, level+1, sum)


if __name__ == '__main__':
    lines = sys.stdin.readlines()
    #lines = open("1.in").readlines()
    lst = [int(i) for i in list(lines[1][:-1].split(" "))]

    check_possible(lst, 2, 1, 0)
